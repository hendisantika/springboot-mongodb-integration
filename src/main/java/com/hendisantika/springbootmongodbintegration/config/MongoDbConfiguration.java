package com.hendisantika.springbootmongodbintegration.config;

import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mongodb-integration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-18
 * Time: 05:22
 */
@Configuration
public class MongoDbConfiguration {

//    private static final Logger logger = LoggerFactory.getLogger(MongoDbConfiguration.class);
//    MongodStarter starter = MongodStarter.getDefaultInstance();
//    MongodExecutable mongodExecutable;
//    @Value("${spring.data.mongodb.host}")
//    private String mongoUrl;
//    @Value("${spring.data.mongodb.database}")
//    private String mongoDbName;
//    @Value("${spring.data.mongodb.port}")
//    private int mongoPort;
//
//    @Bean
//    public MongoTemplate mongoTemplate() throws IOException {
//
//        logger.info("Starting embeded mongo db.................................");
//        EmbeddedMongoFactoryBean mongoFactoryBean = new EmbeddedMongoFactoryBean();
//        mongoFactoryBean.setBindIp(mongoUrl);
//        MongoClient mongoClient = mongoFactoryBean.getObject();
//        MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, mongoDbName);
//        return mongoTemplate;
//    }


}

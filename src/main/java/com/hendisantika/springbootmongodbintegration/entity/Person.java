package com.hendisantika.springbootmongodbintegration.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mongodb-integration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-18
 * Time: 05:21
 */
public class Person {

    public String id;
    public String name;
    public String surname;

    public Person() {

    }

    public Person(String name, String surname) {
        super();
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {

        return " ID : " + this.id + " - Name: " + this.name + " - Surname: " + this.surname;
    }

}

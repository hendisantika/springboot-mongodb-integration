package com.hendisantika.springbootmongodbintegration.repository;

import com.hendisantika.springbootmongodbintegration.entity.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-mongodb-integration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-06-18
 * Time: 05:21
 */
@Repository
public interface PersonRepository extends MongoRepository<Person, String> {

    Person findByName(String name);

    List<Person> findBySurname(String surname);
}